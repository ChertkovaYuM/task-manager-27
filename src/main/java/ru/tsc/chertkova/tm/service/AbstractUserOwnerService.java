package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.api.service.IUserOwnerService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.UserIdEmptyException;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnerService(@Nullable final R repository) {
        super(repository);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(model).orElseThrow(() -> new ModelNotFoundException());
        return repository.add(userId, model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        repository.clear(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        return repository.findById(userId, id);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(model).orElseThrow(() -> new ModelNotFoundException());
        return repository.remove(userId, model);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        if (sort == null) return repository.findAll(userId);
        return repository.findAll(userId, sort);
    }

}
